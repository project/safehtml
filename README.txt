Safe HTML
---------

Description
-----------

Safe HTML is a module that filter the input before the content is stored in the
database. Unlike Drupal basic filtering system, Safe HTML filter the form post
and perform code cleaning before the content is stored on the site backend.

Safe HTML must be enabled on the Input formats section as a filter for the
desired input format. Safe HTML cannot be used in conjunction with PHP Parser
Filter because Safe HTML will strip any PHP code. Safe HTML can be used together
with HTML Filter in order to limit HTML tags to an allowed array.

Administrators and allowed users can define custom PHP code to perform
additional tasks on the form input. The site administrator can define what kind
of custom transformation may occur on the form content. He must appy these
transformations to a variable named $html.

The module is based on SafeHTML, http://pixel-apes.com/safehtml a
project leaded by Roman Ivanov. This module strips down all potentially
dangerous content within HTML:

* opening tag without its closing tag
* closing tag without its opening tag;
* resolving cases like <p><em>abc</p></em>;
* strip any of these tags: "base", "basefont", "head", "html", "body", "applet",
"object", "iframe", "frame", "frameset", "script", "layer", "ilayer", "embed",
"bgsound", "link", "meta", "style", "title", "blink", "xml" etc.
* any of these attributes: on*, data*, dynsrc
* javascript:/vbscript:/about: etc. protocols
* expression/behavior etc. in styles
* any other active content

It also tries to convert code to XHTML valid, but htmltidy is far better
solution for this task.

Install
-------

1. Copy the "safehtml" directory under "modules/" or "sites/all/modules";
2. Go to "admin/build/modules" and eneble "Safe HTML" module;
3. Go to "admin/settings/filters", choose an input format to configure. Enable
Safe HTML as filter for this input format. Typically you should consider to
enable Safe HTML as a filter to "Filtered HTML" input format. You can disable
"HTML filter" because Safe HTML will do that task too.
4. Go to "admin/content/safehtml" in order to add additonal filtering options
when forms are submitted.

